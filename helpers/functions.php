<?php

/**
 * ds: dump-show function that allows preformatting arrays 
 * and all kind of outputs as a result of any function in the application
 * 
 * @param $array
 */
function show_output($array) {
	echo "<pre>";
	print_r($array);
	echo "</pre>";
	
}


/** Revisar si Magic Quotes esta activado para desactivarlo **/
function strip_slashes_deep($value) {
    $value = is_array($value) ? array_map('strip_slashes_deep', $value) : stripslashes($value);
    return $value;
}