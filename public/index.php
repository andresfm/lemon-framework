<?php

/**
 * Lemon Framework
 * A Lightweight PHP Framework that combines a few of every of them in the world.
 *
 * @package		Lemon
 * @author 		Andres Fdo. Moreno J. <andres.fernando08@gmail.com>
 */

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)));

require ROOT . DS . 'config' . DS . 'constants.php';


if (isset($_GET['uri'])) {
	$uri = $_GET['uri'];	
} else {
	$uri = DEFAULT_CONTROLLER;
}


require ROOT . DS . 'system' . DS . 'bootstrapping.php';