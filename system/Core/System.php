<?php
namespace Lemon\Core;

use App\Controllers;

class System
{
	private $_env;
	private $_globals = array();
	
	private $router;


	public function __construct()
	{
		$this->router = new Router();
		
		$this->_env = DEVELOPMENT_ENVIRONMENT;
		$this->_globals = array('_SESSION', '_POST', '_GET', '_COOKIE', '_REQUEST', '_SERVER', '_ENV', '_FILES');
	}


	public function set_environment_app()
	{
		if (DEVELOPMENT_ENVIRONMENT == $this->_env) {
		    error_reporting(E_ALL);
		    ini_set('display_errors','On');
		} else {
		    error_reporting(E_ALL);
		    ini_set('display_errors','Off');
		    ini_set('log_errors', 'On');
		    ini_set('error_log', ROOT.DS.'temp'.DS.'logs'.DS.'error.log');
		}

	}


	public function remove_magic_quotes()
	{
		if ( get_magic_quotes_gpc() ) {
		    $_GET    = strip_slashes_deep($_GET   );
		    $_POST   = strip_slashes_deep($_POST  );
		    $_COOKIE = strip_slashes_deep($_COOKIE);
		}
	}


	public function unregister_globals()
	{
		if (ini_get('register_globals')) {
	        foreach ($this->_globals as $value) {
	            foreach ($GLOBALS[$value] as $key => $var) {
	                if ($var === $GLOBALS[$key]) {
	                    unset($GLOBALS[$key]);
	                }
	            }
	        }

    	}

	}

	
	public function configure() 
	{
 
 		global $uri;

		$uri_array = $this->router->check_uri($uri);
		//ds($uri_array);
		
		/* $uri_array: index 0 => $controller
		 *			   index 1 => $action (or method)
		 * 			   index 2 => $queryString (params)
		 */
		
		$this->router->check_controller($uri_array[0], $uri_array[1], $uri_array[2]);
		
	}




	
}