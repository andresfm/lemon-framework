<?php
namespace Lemon\Core;

use Tale\Jade;

class TemplateEngine
{
    private $_template;
    
    
    public function __construct()
    {
        $this->_template = new Jade\Renderer([
                                                'cache_path' => ROOT . DS . 'temp' . DS . 'cache',
                                                'paths' => [ROOT . DS . 'application' . DS . 'Views',]
                                            ]);
        
    }


    public function _view($view, $arraOptions=array())
    {
        return $this->_template->render($view, $arraOptions);
    }
    
    
}