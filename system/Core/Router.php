<?php
namespace Lemon\Core;

use App\Controllers;


class Router
{

    private $default_method = '';
    

    public function __construct()
    {
        $this->default_method = DEFAULT_METHOD;
        
    }


    public function check_controller($ctrl='', $act='', $qs=array())
    {
        if ( strpos($ctrl, 'Generator') !== FALSE )
            $class = "App\\Controllers\\Generators\\" . $ctrl;
        else
            $class = "App\\Controllers\\" . $ctrl;

        if (class_exists($class)) {
            $dispatch = new $class($ctrl, $act);

            //var_dump(class_exists("App\\Controllers\\".$controller)); // bool(true)
            //ds($dispatch);

            if ( (int)method_exists($dispatch, $act) ) {
                echo call_user_func_array(array($dispatch, $act), $qs);
            } else {
                echo "Method '".$act."' on ".$ctrl." does not exists";
            }

        } else {
            echo $ctrl." does not exists";
        }

    }

    

    public function check_uri($uri='')
    {

        if (substr(trim($uri), -1) == '/')
            $uri = substr(trim($uri), 0, strlen(trim($uri))-1);

        $urlArray = array();
        $urlArray = explode("/", $uri);


        if ( count($urlArray) > 1 ) {
            $controller = $urlArray[0];
            array_shift($urlArray);
            $action = trim($urlArray[0]);
            array_shift($urlArray);
            $queryString = $urlArray;
        } else {
            $controller = $urlArray[0];
            $action = $this->default_method;
            $queryString = array();
        }

        $controllerName = $controller;
        $controller = ucwords($controller);
        //$model = rtrim($controller, 's');
        $controller .= 'Controller';
        
        $response = array($controller, $action, $queryString);
        
        return $response;
    }




}