<?php
namespace Lemon\Database;

use PDO;
use PDOException;

class DB_Connector
{
    protected $db;
    
    
    public function __construct()
    {
        $this->_initialize();
    }
    
    
    private function _initialize()
    {
        try {
            $this->db = new PDO(DB_DRIVER.":host=".DB_HOST.";dbname=".DB_NAME,
                                DB_USER, DB_PASSWORD);
            $this->db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
            //$this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $this->db->exec("set names utf8");

        } catch (PDOException $e) {
            ds('Error trying connecting to the database: <br />' . $e->getMessage());
            die();
        }
        
    }
    
    
    protected function getConnection()
    {
        return $this->db;
    }
    
    protected function destroyConnection()
    {
        $this->db = null;
    }
    
    
    
    

}