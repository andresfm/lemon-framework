<?php
namespace Lemon\Database;


interface DB_InterfaceAdapter
{


    /**
     * @param $query
     * @param null $conditions
     * @return mixed
     */
    function execute($query, $conditions=null);


    /**
     * @param $table
     * @param array $fields
     * @param array $where
     * @param string $order
     * @param null $limit
     * @param null $offset
     * @return mixed
     */
    function select($table, $fields=[], $where=[], $order='', $limit=null, $offset=null);


    /**
     * @param $table
     * @param array $data
     * @return mixed
     */
    function insert($table, $data=array());


    /**
     * @param $table
     * @param array $data
     * @param array $conditions
     * @return mixed
     */
    function update($table, $data=array(), $conditions=[]);


    /**
     * @param $table
     * @param array $conditions
     * @return mixed
     */
    function delete($table, $conditions=[]);

    /**
     * @return mixed
     */
    function fetch();
    
    
}