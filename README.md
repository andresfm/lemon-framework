# Lemon
A lightweight PHP framework based on MVC Architecture from scratch inspired in the best of several frameworks

# About Lemon
. . .

# How to getting started
. . .

# About security-holes
Tell me if you find a security vulnerability in Lemon, sending me an e-mail at <a href="mailto:andres.fernando08@gmail.com">andres.fernando08@gmail.com</a>

# License
The Lemon framework is released as a open-source software licensed under the [MIT License](http://opensource.org/licenses/MIT)
